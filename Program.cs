﻿using ClosedXML.Excel;
using CommandLine;
using System;
using System.IO;
using System.Linq;

namespace XLSX_Keywords_Replacer
{
    public class Program
    {
        public class Options
        {
            [Option('f', "file", Required = true, HelpText = "Select xlsx file path")]
            public string File { get; set; }

            [Option('k', "keyword", Required = true, HelpText = "Keyword to be replaced")]
            public string Keyword { get; set; }

            [Option('r', "rep-keyword", Required = true, HelpText = "Replaced keyword (new)")]
            public string Destination { get; set; }

            [Option('o', "override-cell", Required = false, Default = false, HelpText = "If not selected, words are replaced. When selected, the entire cell is replaced.")]
            public bool OverrideCell { get; set; }
        }

        static void Main(string[] args)
        {
            Parser.Default.ParseArguments<Options>(args).WithParsed<Options>(o =>
            {
                Console.WriteLine($"File: {o.File} [{o.Keyword} -> {o.Destination}]");
                if (!File.Exists(o.File))
                {
                    Console.WriteLine($"Not found xlsx file. {o.File}");
                    return;
                }

                try
                {
                    var xlsx = new XLWorkbook(o.File);
                    foreach (var ws in xlsx.Worksheets)
                    {
                        Console.WriteLine($"{ws.Name} sheet");
                        var finds = ws.Search(o.Keyword);
                        Console.WriteLine($"finded count: {finds.Count()}");
                        int replacedCount = 0;
                        foreach (var c in finds)
                        {
                            if (o.OverrideCell)
                            {
                                c.SetValue(o.Destination);
                                replacedCount++;
                            }
                            else
                            {
                                if (c.TryGetValue(out string val))
                                {
                                    var v = val.Replace(o.Keyword, o.Destination);
                                    c.SetValue(v);
                                    replacedCount++;
                                }
                            }
                        }
                        Console.WriteLine($"Replaced count: {replacedCount}");
                    }
                    xlsx.Save();
                }
                catch (Exception e) { Console.WriteLine(e); }
            });
        }
    }
}
